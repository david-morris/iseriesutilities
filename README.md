# README #

iSeries Utilities provides useful features that can be used on an iSeries AS/400. The utilities include:

* Sending emails
* Creating CSV files form an SQL Select statement
* Encrypting ISF files via PGP
* Converting spooled files to PDF

* Version 1.00

### Summary of set up ###
This repository is an Eclipse project.

* Configuration

### Dependencies ###

* Eclipse
* Java Runtime Environment (JRE)
* Bouncy Castle 1.4.7
* iText 2.1.7
* JAF 1.1
* Java Mail 1.4
* JTOpen 7.5.1