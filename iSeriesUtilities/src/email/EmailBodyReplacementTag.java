package email;

/**
 * The email body can have replacement tags which will be scanned-and-replaced
 * before sending the email.
 * <p>
 * eg. Find '&CLIENTNAME&' and replace it with 'John Smith'.
 * <p>
 * Any naming convention can be used for the tags.
 * 
 * @author David Morris.
 */
public class EmailBodyReplacementTag {
	private String from = " ";
	private String to = " ";

	/**
	 * Creates an instance of <code>EmailBodyReplacement</code> with replacement
	 * parameters 'from' and 'to'.
	 * 
	 * @param from
	 *            The string to find
	 * @param to
	 *            The string to use as the replacement
	 */
	EmailBodyReplacementTag(String from, String to) {
		this.from = from;
		this.to = to;
	}

	/**
	 * Creates an instance of <code>EmailBodyReplacement</code>
	 * <b><u>without</u></b> replacement parameters 'from' and 'to'.
	 */
	EmailBodyReplacementTag() {
	}

	/**
	 * Gets the 'from' string (the string to find).
	 * 
	 * @return from The string to find
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * Sets the 'from' string (the string to find).
	 * 
	 * @param from
	 *            The string to find
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * Gets the 'to' string (the string to use as the replacement).
	 * 
	 * @return to The string to use as the replacement
	 */
	public String getTo() {
		return to;
	}

	/**
	 * Sets the 'to' string (the string to use as the replacement).
	 * 
	 * @param to
	 *            The string to use as the replacement
	 */
	public void setTo(String to) {
		this.to = to;
	}

}