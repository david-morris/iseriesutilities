package email;

/**
 * The email address consists of a type, an address and a name.
 * <p>
 * The type can be 'FROM', 'TO', 'CC', 'BCC' or 'REPLYTO'.
 * <p>
 * The address is in the form of 'john.smith@isp.com'.
 * <p>
 * The name is in the form of 'John Smith'.
 * 
 * @author David Morris.
 */
public class EmailAddress {
	private String type = " ";
	private String address = " ";
	private String name = " ";

	/**
	 * Creates an instance of <code>EmailAddress</code> with the type, address
	 * and name.
	 * 
	 * @param type
	 *            The type of email address to add (FROM, TO, CC, BCC or
	 *            REPLYTO).
	 * @param address
	 *            The email address to add (eg john.smith@isp.com).
	 * @param name
	 *            The name of the person (eg John Smith). Optional. If not
	 *            supplied, 'address' will be used as the name.
	 */
	EmailAddress(String type, String address, String name) {
		this.type = type;
		this.address = address;
		if (name.trim().equals("")) {
			this.name = address;
		} else {
			this.name = name;
		}

	}

	/**
	 * Creates an instance of <code>EmailAddress</code> <b><u>without</u></b>
	 * the type, address or name.
	 */
	EmailAddress() {
	}

	/**
	 * Gets the 'address' of the email address.
	 * 
	 * @return String The email address (eg john.smith@isp.com).
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the 'address' of the email address. If the 'name' has not already
	 * been set then 'address' will be applied to the 'name' as well.
	 * 
	 * @param address
	 *            The email address (eg john.smith@isp.com).
	 */
	public void setAddress(String address) {
		this.address = address;
		if (this.name.trim().equals("")) {
			this.name = address;
		}
	}

	/**
	 * Gets the 'name' of the email address.
	 * 
	 * @return String Name of the email address (eg John Smith).
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the 'name' of the email address.
	 * 
	 * @param name
	 *            Name (eg John Smith).
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the 'type' of email address.
	 * 
	 * @return type The type of email (eg FROM, TO, CC, BCC or REPLYTO).
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the 'type' of email address.
	 * 
	 * @param type
	 *            The type of email (eg FROM, TO, CC, BCC or REPLYTO).
	 */
	public void setType(String type) {
		this.type = type;
	}

}
