package email;

import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Array;
import java.util.Properties;
import java.util.Vector;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Uses the JavaMail routines to construct and send an email.
 * <p>
 * The email can:
 * <ul>
 * <li>be sent to multiple recipients</li>
 * <li>handle CC, BCC and REPLYTO directives</li>
 * <li>handle attachments</li>
 * <li>be plain text or HTML</li>
 * </ul>
 * 
 * Example Usage:
 * 
 * <pre>
 * try {
 * 	IseriesEmail ie = new IseriesEmail();
 * 	ie.addAddress(&quot;FROM&quot;, &quot;travel.service@qbe.com&quot;, &quot;&quot;);
 * 	ie.addAddress(&quot;TO&quot;, &quot;david.morris@qbe.com.au&quot;, &quot;&quot;);
 * 	ie.setSubject(&quot;Sample including substitution variables&quot;);
 * 	ie.setBodyFile(&quot;/template.html&quot;, &quot;text/html&quot;);
 * 	ie.addFileAttachment(&quot;/logo.gif&quot;, &quot;logo.gif&quot;, &quot;image/gif&quot;, true);
 * 	ie.addBodyReplacementTag(&quot;_NAME_&quot;, &quot;John Smith&quot;);
 * 	ie.setSmtpHost(&quot;smtp1.qbe.com&quot;);
 * 	ie.setSmtpPort(&quot;25&quot;);
 * 
 * 	ie.send();
 * } catch (EmailException e) {
 * 	System.out.println(&quot;Error &quot; + e);
 * }
 * </pre>
 * 
 * 
 * @author David Morris.
 */
public class Email {
	private String subject = " ";
	private Vector vReplacement = new Vector();
	private Vector vAddress = new Vector();
	private EmailBody body = new EmailBody();
	private Vector vFileAttachment = new Vector();
	private String smtpHost = " ";
	private String smtpPort = " ";

	public static final String TO = "TO";
	public static final String FROM = "FROM";
	public static final String CC = "CC";
	public static final String BCC = "BCC";
	public static final String REPLYTO = "REPLYTO";

	/**
	 * Constructs an instance of <code>IseriesEmail</code>.
	 */
	public Email() {
	}

	/**
	 * Gets the subject of the email.
	 * 
	 * @return subject The subject of the email.
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Sets the subject of the email.
	 * 
	 * @param subject
	 *            The subject of the email.
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * Adds the address of the email including the type of address, the address
	 * itself and the name.
	 * 
	 * @param type
	 *            The type of address (FROM, TO, CC, BCC, REPLYTO)
	 * @param address
	 *            The address (eg john.smith@isp.com)
	 * @param name
	 *            The name (eg John Smith)
	 * 
	 * @throws EmailException
	 *             Generic Email Exception routine.
	 */
	public void addAddress(String type, String address, String name) throws EmailException {

		if (address.trim().equals("")) {
			throw new EmailException("Email address cannot be blank.");
		}
		if (type.trim().equals("")) {
			throw new EmailException("Email type cannot be blank.");
		}

		EmailAddress ea = new EmailAddress(type.trim(), address.trim(), name.trim());
		vAddress.add(ea);
	}

	/**
	 * Gets the 'nth' vector <code>EmailAddress</code>. There is support for
	 * multiple email addresses and this is handled by use a vector to store all
	 * <code>EmailAddress</code> instances.
	 * 
	 * @param i
	 *            The index (or 'nth' vector-element) to get.
	 * @return EmailAddress <code>EmailAddress</code> object.
	 */
	public EmailAddress getAddress(int i) {
		return (EmailAddress) vAddress.get(i);
	}

	/**
	 * Adds a file attachment to the email.
	 * 
	 * @param longName
	 *            The location of the physical attachment
	 * @param shortName
	 *            The name of the attachment @TODO - Why do we have a name?
	 * @param contentType
	 *            A valid registered MIME content type
	 * @param embed
	 *            A boolean operator to determine if the attachment is to be
	 *            embedded
	 * 
	 * @throws EmailException
	 *             Generic Email Exception routine.
	 */
	public void addFileAttachment(String longName, String shortName, String contentType,
			boolean embed) throws EmailException {

		File file = new File(longName);
		if (!file.exists()) {
			throw new EmailException("Attachment file : " + longName + " not found.");
		}

		EmailAttachment ea = new EmailAttachment();
		ea.setLongName(longName);
		if (shortName.trim().equals("")) {
			ea.setShortName(longName);
		} else {
			ea.setShortName(shortName);
		}
		ea.setContentType(contentType);
		ea.setEmbed(embed);
		vFileAttachment.add(ea);
	}

	/**
	 * Sets the Body of the email.
	 * 
	 * @param text
	 *            The text to be used in the body of the email.
	 * @param contentType
	 *            The content type of the text. ('text/plan' or 'html/plain').
	 */
	// @TODO - Source is a bad name
	public void setBodyText(String text, String contentType) {
		if (contentType.trim().equals("")) {
			contentType = "text/plain";
		}

		body.setSourceType("TEXT");
		body.setSource(text);
		body.setContentType(contentType);
	}

	/**
	 * 
	 * @param text
	 *            Some text to add
	 */
	// @TODO - this is mine. Is it still relevant and still called the right
	// thing?
	// @TODO - Source is a bad name
	public void addBodyText(String text, String contentType) {
		if (contentType.trim().equals("")) {
			contentType = "text/plain";
		}

		body.setSourceType("TEXT");
		body.addSource(text);
		body.setContentType(contentType);
	}

	/**
	 * Gets the body of the email via a direct piece of text.
	 * 
	 * @return body The body of the email
	 */
	// @TODO - Source is a bad name
	public String getBodyText() {
		return body.getSource();
	}

	/**
	 * Sets the body of the email via a file.
	 * 
	 * @param file
	 *            Name of the file to be used as the body of the email
	 * @param contentType
	 *            Content type of the email ('text/plain' or 'html/plain')
	 * 
	 * @throws EmailException
	 *             Generic Email Exception routine.
	 */
	// @TODO - Source is a bad name
	public void setBodyFile(String file, String contentType) throws EmailException {

		File inputFile = new File(file);
		if (!inputFile.exists()) {
			throw new EmailException("Body file : " + file + " not found.");
		}

		if (contentType.trim().equals("")) {
			contentType = "text/plain";
		}

		body.setSourceType("FILE");
		body.setSource(file);
		body.setContentType(contentType);
	}

	/**
	 * Adds a body replacement tag.
	 * 
	 * The body can include replacement-tags that will then be
	 * scanned-and-replaced prior to the email being sent out.
	 * 
	 * @param from
	 *            Text to be found
	 * @param to
	 *            Text to use as the replacement
	 * 
	 * @throws EmailException
	 *             Generic Email Exception routine.
	 */
	public void addBodyReplacementTag(String from, String to) throws EmailException {

		// if (from.trim().equals("")) {
		// throw new EmailException("Replacement From cannot be blank.");
		// }
		// if (to.trim().equals("")) {
		// throw new EmailException("Replacement To cannot be blank.");
		// }

		// EmailBodyReplacementTag r = new EmailBodyReplacementTag(from.trim(),
		// to.trim());

		if (!from.trim().equals("")) {
			EmailBodyReplacementTag r = new EmailBodyReplacementTag(from, to);
			vReplacement.add(r);
		}
	}

	/**
	 * Gets the <code>EmailBody</code> of the email.
	 * 
	 * @return EmailBody <code>EmailBody</code> of the email.
	 */
	public EmailBody getBody() {
		return body;
	}

	/**
	 * Gets the SMTP host to use to send the email.
	 * 
	 * @return smtpHost The SMTP host to use to send the email
	 */
	public String getSmtpHost() {
		return smtpHost;
	}

	/**
	 * Sets the SMTP host to use to send the email.
	 * 
	 * @param smtpHost
	 *            The SMTP host to use to send the email
	 */
	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	/**
	 * Gets the SMTP port to use to send the email.
	 * 
	 * @return smtpPort The SMTP port to use to send the email
	 */
	public String getSmtpPort() {
		return smtpPort;
	}

	/**
	 * Sets the SMTP port to use to send the email.
	 * 
	 * @param smtpPort
	 *            The SMTP port to use to send the email
	 */
	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}

	/**
	 * Sends the email with all the components that have been added.
	 * 
	 * @return String
	 * @throws EmailException
	 *             Generic Email Exception routine.
	 */
	// @TODO - what is being returned?
	public String send() throws EmailException {
		String result = "";
		try {

			boolean hasToAddress = false;
			boolean hasFromAddress = false;
			
			// Put the smtpHost and smtpPort into a Properties object because
			// these are then used by javax.mail to create a Session object
			Properties systemProperty = System.getProperties();
			systemProperty.put("mail.smtp.host", smtpHost);
			systemProperty.put("mail.smtp.port", smtpPort);
			systemProperty.put("mail.smtp.connectiontimeout","20000"); // connection timeout in milliseconds
			systemProperty.put("mail.smtp.timeout","20000"); // IO timeout in milliseconds
			systemProperty.put("mail.smtp.writetimeout","20000"); 

			// Create a javax.mail.Session object and create a Message object
			Session session = Session.getInstance(systemProperty);
			Message message = new MimeMessage(session);

			// Go through all of the email addresses and prime the TO, CC and
			// BCC in the javax.mail.Message object
			EmailAddress ea = new EmailAddress();
			for (int i = 0; i < vAddress.size(); i++) {
				ea = (EmailAddress) vAddress.get(i);
				if (!ea.getAddress().equals(" ")) {
					if (ea.getType().equals(Email.TO)) {
						message.addRecipient(Message.RecipientType.TO,
								new InternetAddress(ea.getAddress(), ea.getName()));
						hasToAddress = true;
					}
					if (ea.getType().equals(Email.CC)) {
						message.addRecipient(Message.RecipientType.CC,
								new InternetAddress(ea.getAddress(), ea.getName()));
					}
					if (ea.getType().equals(Email.BCC)) {
						message.addRecipient(Message.RecipientType.BCC,
								new InternetAddress(ea.getAddress(), ea.getName()));
					}
				}
			}

			// Go through all of the email addresses and store all REPLYTO
			// entries in an array
			int x = 0;
			InternetAddress[] replyToArray = new InternetAddress[0];
			for (int i = 0; i < vAddress.size(); i++) {
				ea = (EmailAddress) vAddress.get(i);

				if (ea.getType().equals(Email.REPLYTO) && !ea.getAddress().trim().equals("")) {
					replyToArray = (InternetAddress[]) increaseArray(replyToArray);
					replyToArray[x] = new InternetAddress(ea.getAddress(), ea.getName());
					x = x + 1;
				}
			}

			// If the REPLYTO array has any elements then prime the REPLYTO in
			// the javax.mail.Message object
			if (replyToArray.length > 0) {
				message.setReplyTo(replyToArray);
			}

			// Go through all of the email addresses and store all FROM entries
			// in an array
			x = 0;
			InternetAddress[] fromArray = new InternetAddress[0];
			for (int i = 0; i < vAddress.size(); i++) {
				ea = (EmailAddress) vAddress.get(i);
				if (ea.getType().equals(Email.FROM) && !ea.getAddress().trim().equals("")) {
					fromArray = (InternetAddress[]) increaseArray(fromArray);
					fromArray[x] = new InternetAddress(ea.getAddress(), ea.getName());
					hasFromAddress = true;
				}
			}

			// If the FROM array has any elements then prime the FROM in the
			// javax.mail.Message object
			if (fromArray.length > 0) {
				message.addFrom(fromArray);
			}

			if (hasFromAddress == false || hasToAddress == false) {
				return "From or To address missing";
			}
			
			// Set the subject of the email in the javax.mail.Message object
			message.setSubject(this.subject);

			// Load the file to be used as the body of the email into a
			// StringBuffer
			java.lang.StringBuffer textBuffer = new java.lang.StringBuffer();
			if (body.getSourceType().equals("FILE")) {
				File inputFile = new File(body.getSource());
				FileReader in = new FileReader(inputFile);
				int c;
				while ((c = in.read()) != -1) {
					char c1 = (char) c;
					textBuffer.append(c1);
				}
				in.close();
			}

			// If the body has had some text added directly (not via a file)
			// then
			// append that to the StringBuffer now.
			if (body.getSourceType().equals("TEXT")) {
				textBuffer.append(new java.lang.StringBuffer(body.getSource()));
			}

			// Process all pending email body replacement tags
			// ie find all replacement-tags and replace them with required text
			EmailBodyReplacementTag er = new EmailBodyReplacementTag();
			x = 0;
			for (int i = 0; i < vReplacement.size(); i++) {
				er = (EmailBodyReplacementTag) vReplacement.get(i);
				textBuffer = replace(textBuffer.toString(), er.getFrom(), er.getTo());
			}

			// If there are attachments we need to create and send a multipart
			// email
			// so create the objects we need now in case we need them later on
			Multipart multipart = new MimeMultipart();
			BodyPart messageBodyPart = new MimeBodyPart();

			// If the body is NOT empty and there is an attachment then we need
			// to setup a multipart email
			if (textBuffer.length() > 0 && !vFileAttachment.isEmpty()) {
				messageBodyPart.setContent(textBuffer + "\n\n", body.getContentType());
				multipart.addBodyPart(messageBodyPart);

				// If the body is NOT empty set the javax.mail.Message object
			} else if (textBuffer.length() > 0) {
				message.setContent(textBuffer + "\n\n", body.getContentType());

				// If the body IS empty so set the java.mail.Message object to
				// be plain text
			} else {
				message.setContent(" ", "text/plain");
			}

			// Add all email attachments to the multipart email
			EmailAttachment emailAttch = new EmailAttachment();
			for (int i = 0; i < vFileAttachment.size(); i++) {
				emailAttch = (EmailAttachment) vFileAttachment.get(i);
				messageBodyPart = new MimeBodyPart();
				messageBodyPart.setText("\n");

				// Set the location of where to get the attachment from
				DataSource source = new FileDataSource(" ");
				source = new FileDataSource(emailAttch.getLongName());
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setHeader("Content-Type", emailAttch.getContentType());

				// @TODO - trying something new here
				if (emailAttch.getEmbed()) {
					//String conID = "<a" + (i + 1) + ">";
					//messageBodyPart.setHeader("Content-ID", conID);
				}

				messageBodyPart.setFileName(emailAttch.getShortName());
				if (emailAttch.getEmbed()) {
					messageBodyPart.setDisposition(MimeBodyPart.INLINE);
				}
				multipart.addBodyPart(messageBodyPart);
			}

			// If there are attachments tell the javax.mail.Message object we
			// are sending a multipart email
			if (!vFileAttachment.isEmpty()) {
				message.setContent(multipart);
			}

			// Send the email now that we have completely constructed it
			Transport.send(message);
			System.out.println("At end of send()");

		} catch (SendFailedException sfe) {
			result = getStackTrace(sfe);
		} catch (MessagingException me) {
			result = getStackTrace(me);
		} catch (Exception e) {
			result = getStackTrace(e);			
		} finally {
			if (result.length() > 0) throw new EmailException(result);
		}
		
		return result;
	}

	/**
	 * Increases array size dynamically.
	 * 
	 * @param source
	 * @return
	 */
	static Object increaseArray(Object source) {
		int sourceLength = Array.getLength(source);
		Class arrayClass = source.getClass();
		Class componentClass = arrayClass.getComponentType();
		Object result = Array.newInstance(componentClass, sourceLength + 1);
		System.arraycopy(source, 0, result, 0, sourceLength);
		return result;
	}

	/**
	 * Formats the error stack caught during an exception.
	 * 
	 * @param aThrowable
	 * 
	 * @return String Error Stack
	 */
	static String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString();
	}

	/**
	 * Replaces a String with another String.
	 * 
	 * @param str
	 *            String to look in
	 * @param pattern
	 *            Pattern to look for
	 * @param replace
	 *            Replacement string
	 * 
	 * @return StringBuffer Resultant String after the replacement
	 */
	static StringBuffer replace(String str, String pattern, String replace) {
		int s = 0;
		int e = 0;
		StringBuffer result = new StringBuffer();

		while ((e = str.indexOf(pattern, s)) >= 0) {
			result.append(str.substring(s, e));
			result.append(replace);
			s = e + pattern.length();
		}
		result.append(str.substring(s));
		return result;
	}

}
