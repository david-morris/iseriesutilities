package email;

/**
 * The email body can be built via an external file. It can also be built one
 * line at a time.
 * 
 * @author David Morris.
 */

// @TODO - document the contentTypes available
// @TODO - document the sourceTypes available
// @TODO - is "source" a good name if used to add one line instead of a file
public class EmailBody {

	private String sourceType = " ";
	private String source = " ";
	private String contentType = " ";

	/**
	 * Creates a new instance of <code>EmailBody</code>.
	 */
	public EmailBody() {
	}

	/**
	 * Gets the source type of the email body.
	 * 
	 * @return sourceType The source type of the email body
	 */
	public java.lang.String getSourceType() {
		return sourceType;
	}

	/**
	 * Sets the source type of the email body.
	 * 
	 * @param sourceType
	 *            The source type of the email body
	 */
	public void setSourceType(java.lang.String sourceType) {
		this.sourceType = sourceType;
	}

	/**
	 * Gets the source of the email body.
	 * 
	 * @return source The source of the email body.
	 */
	public java.lang.String getSource() {
		return source;
	}

	// @TOD - Is this really relevant if just one line of text is being added
	// instead of using a file
	/**
	 * Sets the source of the email body.
	 * 
	 * @param source
	 *            The source of the email body
	 */
	public void setSource(java.lang.String source) {
		this.source = source;
	}

	// @TODO - this is mine. addSource should be change to reflect what it does.
	// eg addLine
	/**
	 * 
	 * @param source
	 *            Text to be appended to the end of the body.
	 */
	public void addSource(java.lang.String source) {
		this.source = this.source + "\n" + source;
	}

	/**
	 * Gets the content type of the email body.
	 * 
	 * @return contentType The content type of the email body
	 */
	public java.lang.String getContentType() {
		return contentType;
	}

	/**
	 * Sets the content type of the email body.
	 * 
	 * @param contentType
	 *            The content type of the email body
	 */
	public void setContentType(java.lang.String contentType) {
		this.contentType = contentType;
	}

}