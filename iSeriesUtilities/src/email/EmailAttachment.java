package email;

/**
 * The email attachment consists of a longName and a shortName, a contentType
 * and a boolean operator to indicate if the attachment should be embedded or
 * treated merely as an attachment.
 * <p>
 * The longName is in the form of '/path1/path2/name.txt'.<br>
 * The shortName will usually be a simple name, not including any directory
 * components.
 * <p>
 * The contentType is any one of the registered MIME types such as text/html,
 * image/png, image/gif, video/mpeg, text/css, audio/basic.
 * <p>
 * The embed flag is a boolean operator to indicate if the attachment is to be
 * embedded in the email or to be treated as merely an attachment.
 * 
 * @author David Morris.
 */

public class EmailAttachment {

	private String longName = " ";
	private String shortName = " ";
	private String contentType = " ";
	private boolean embed = false;

	/**
	 * Creates an instance of <code>EmailAttachment</code> to be used in the
	 * email.
	 */
	public EmailAttachment() {
	}

	/**
	 * Gets the location of the attachment.
	 * 
	 * @return String The location of the attachment.
	 */
	public String getLongName() {
		return longName;
	}

	/**
	 * Sets the location of the attachment.
	 * 
	 * @param longName
	 *            The location of the attachment.
	 */
	public void setLongName(String longName) {
		this.longName = longName;
		;
	}

	/**
	 * Gets the name of the attachment.
	 * 
	 * @return String The name of the attachment.
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * Sets the name of the attachment.
	 * 
	 * @param shortName
	 *            The name of the attachment.
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * Gets the content type of the attachment.
	 * 
	 * @return String The content type of the attachment.
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * Sets the content type of the attachment.
	 * 
	 * @param contentType
	 *            The content type of the attachment.
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * Gets the content type of the attachment.
	 * 
	 * @return boolean Flag to determine if the attachment is to be embedded in
	 *         the email or added as an attachment.
	 */
	public boolean getEmbed() {
		return embed;
	}

	/**
	 * Sets the content type of the attachment.
	 * 
	 * @param embed
	 *            Flag to determine if the attachment is to be embedded in the
	 *            email or added as an attachment.
	 */
	public void setEmbed(boolean embed) {
		this.embed = embed;
	}

}
