package email;

/**
 * 
 * @author David Morris.
 * 
 */
public class EmailException extends java.lang.Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8292994211459068084L;

	/**
	 * Creates a new instance of <code>EmailException</code> without detail
	 * message.
	 */
	public EmailException() {
	}

	/**
	 * Constructs an instance of <code>EmailException</code> with the specified
	 * detail message.
	 * 
	 * @param msg
	 *            The detail message.
	 */
	public EmailException(String msg) {
		super(msg);
	}
}