package email;

/**
 * Tests the Email routine by allowing you to invoke it directly by a command
 * line passing in three parameters command line arguments (toAddress smtpHost
 * smtpPort).
 * 
 * @author David Morris
 * 
 */
public class EmailTest {

	/**
	 * @param args
	 *            Command Line arguments
	 */
	public static void main(String[] args) {

		String strTo = "";
		String strSmtpHost = "";
		String strSmtpPort = "";

		if (args.length < 3) {
			System.out.println("IseriesUtilities Email test wrapper");
			System.out.println("-----------------------------------");
			System.out.println("");
			System.out.println("Usage:  iseriesutilities.email.Test <TO> <SmtpHost> <SmptPort>");
			System.out.println("");
			System.out.println("TO       = the email address to send the email to");
			System.out.println("SmtpHost = the SMTP server you will be using");
			System.out.println("SmtpPort = the port of the SMTP server you will be using");
			System.out.println("");
			System.out.println("");
			System.out.println("Example: iseriesutilities.email.test john@isp.com smtp.isp.com 25");
			System.out.println("");
			System.out.println("");
		} else {

			strTo = args[0];
			strSmtpHost = args[1];
			strSmtpPort = args[2];

			System.out.println(".... Starting Mac version");
			try {
				System.out.println(strTo + ", " + strSmtpHost + ", " + strSmtpPort);

				Email ie = new Email();
				ie.addAddress("FROM", "some.one@isp.com", "");
				ie.addAddress("TO", strTo, "");
				ie.setSubject("Sample email");
				ie.addBodyText("Test email", "text/plain");
				ie.addFileAttachment("longname.pdf", "shortName", "contentType", false);
				ie.setSmtpHost(strSmtpHost);
				ie.setSmtpPort(strSmtpPort);

				ie.send();
				System.out.println(".... Finish");
			} catch (EmailException e) {
				System.out.println("Error " + e);
			}
		}
	}
}
