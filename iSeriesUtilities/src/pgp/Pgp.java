package pgp;

import java.io.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Date;
import java.util.Iterator;
import java.io.File;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPLiteralDataGenerator;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.operator.bc.*;

/**
 * Encrypt a file using PGP encryption with a specified public key(s).
 * 
 * This object has two instance variables to hold the name of the input file
 * and the name of the public key(s) to use while encrypting the file.
 * 
 * fileName is a String object including full path to designate
 * the file to be encrypted.
 * 
 * publicKeyFile is a String object of semicolon-separated key files (including
 * full path) to use during encryption. eg "/path1/key1.asc;/path1/key2.asc"
 * 
 * The input file will be encrypted to ALL of the keys which means any one of the
 * key recipients can open the file.
 *
 */
public class Pgp {
	
	/**
	 * Name of file (including full path) that is to be encrypted
	 */
	private String fileName = "";
	
	/**
	 * Semicolon separated list of key files to use during encryption
	 * eg "/path1/key1.asc;/path1/key2.asc"
	 */
	private String pubKeyFile = "";

	/**
	 * Get the file name that is to be encrypted
	 * 
	 * @return fileName The filename that is to be encrypted.
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Set the file name that is to be encrypted
	 * 
	 * @param fileName
	 *            The filename that is to be encrypted
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Get public key file to be used to encrypt the file to. This can be a
	 * semicolon separated list if multiple public key files are to be used.
	 * 
	 * @return publicKeyFile
	 */
	public String getPubKeyFile() {
		return pubKeyFile;
	}

	/**
	 * Set public key file to be used to encrypt the file to. This can be a
	 * semicolon separated list if multiple public key files are to be used.
	 * 
	 * @param publicKeyFile
	 */
	public void setPubKeyFile(String publicKeyFile) {
		this.pubKeyFile = publicKeyFile;
	}

	private final static int BUFFER_SIZE = 1 << 16;

	/**
	 * Encrypt the specified input file to all public keys provided.
	 * 
	 * @return Any errors as a result of the encryption process.
	 */
	public String encryptFile() {

		String result = "";

		try {
			Writer outLog = new OutputStreamWriter(new FileOutputStream("/travel/java/export.log"));
			outLog.write(this.getFileName() + "\n");
			outLog.write(this.getPubKeyFile() + "\n");
			
			// Initialise the security provider
			Security.addProvider(new BouncyCastleProvider());
			outLog.write("1");

			// Initialise encrypted data generator (the new way)
			BcPGPDataEncryptorBuilder builder = new BcPGPDataEncryptorBuilder(
					PGPEncryptedData.CAST5);
			outLog.write("2");
			outLog.close();
			
			builder.setSecureRandom(new SecureRandom());
			PGPEncryptedDataGenerator encryptedDataGenerator = new PGPEncryptedDataGenerator(
					builder);

			// Get all encryption keys and add them to the data generator object
			// The key file can be split by a semicolon (;)
			String[] myKeys = this.getPubKeyFile().split(";");
			for (int i = 0; i <= myKeys.length - 1; i++) {
				FileInputStream pubKey2 = new FileInputStream(myKeys[i]);
				PGPPublicKey encKey2 = readPublicKeyFromCollection(pubKey2);
				encryptedDataGenerator.addMethod(encKey2);
			}

			// get the input file name (without the path)
			File inputFile = new File(this.getFileName());
			String inputFileName = inputFile.getName();

			// create output stream to disk (the physical encrypted file)
			OutputStream out = new FileOutputStream(this.getFileName() + ".pgp");

			// create encrypted output stream (that directs back to physical
			// output file on disk)
			OutputStream encryptedOut = encryptedDataGenerator.open(out, new byte[BUFFER_SIZE]);

			// create compressed output stream (that directs back to encrypted
			// output stream)
			PGPCompressedDataGenerator compressedDataGenerator = new PGPCompressedDataGenerator(
					PGPCompressedData.ZIP);
			OutputStream compressedOut = compressedDataGenerator.open(encryptedOut);

			// create "literal" output stream (that directs back to compressed
			// output stream)
			PGPLiteralDataGenerator literalDataGenerator = new PGPLiteralDataGenerator();
			OutputStream literalOut = literalDataGenerator.open(compressedOut,
					PGPLiteralData.BINARY, inputFileName, new Date(), new byte[BUFFER_SIZE]);

			// create input stream to read our input file
			FileInputStream inputFileStream = new FileInputStream(this.getFileName());

			// read our input file and write to our "literal" output file
			byte[] buf = new byte[BUFFER_SIZE];
			int len;
			while ((len = inputFileStream.read(buf)) > 0) {
				literalOut.write(buf, 0, len);
			}

			// close log file
			//outLog.close();
			
			// close the input file
			inputFileStream.close();

			// close the "literal" output file
			literalOut.close();
			literalDataGenerator.close();

			// close the compressed output file
			compressedOut.close();
			compressedDataGenerator.close();

			// close the encrypted output file
			encryptedOut.close();
			encryptedDataGenerator.close();

			// close the physical output file on disk
			out.close();

		} catch (Exception e) {
			System.out.println(e);
			return e.toString();
		}
		return result;
	}

	/**
	 * Read the public key from a "collection" and return the public key object
	 * as long as the key can be used for encryption.
	 * 
	 * @param input stream of key file
	 * @return Public key object, or null if none found
	 * @throws Exception
	 */
	private static PGPPublicKey readPublicKeyFromCollection(InputStream in) throws Exception {
		in = PGPUtil.getDecoderStream(in);
		PGPPublicKeyRing pkRing = null;
		PGPPublicKeyRingCollection pkCol = new PGPPublicKeyRingCollection(in);

		// iterate through all "key rings" found in the collection
		Iterator it = pkCol.getKeyRings();
		while (it.hasNext()) {
			// get the next available "key ring" in the collection
			pkRing = (PGPPublicKeyRing) it.next();

			// iterate through all public keys
			Iterator pkIt = pkRing.getPublicKeys();
			while (pkIt.hasNext()) {
				// get next available public key
				PGPPublicKey key = (PGPPublicKey) pkIt.next();
				
				// if the public key can be used for encryption then return the key object
				if (key.isEncryptionKey())
					return key;
			}
		}
		return null;
	}

}
