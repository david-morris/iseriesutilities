package pgp;

public class PgpTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//System.out.println(" ... running PgpTest.java ...");
		//System.out.println("classpath=" + System.getProperty("java.class.path"));

		if (args.length < 2) {
			System.out.println("Two parameters required.");
			System.out.println("  1. Name of file to be encrypted (include full path)");
			System.out.println("  2. Public Key File to use for encryption (include full path)");
		} else {

			try {
				System.out.println("fileName = " + args[0]);
				System.out.println("pubKeyFile = " + args[1]);
				Pgp pgp = new Pgp();
				pgp.setFileName(args[0]);
				pgp.setPubKeyFile(args[1]);
				
				String result;
				result = pgp.encryptFile();
				System.out.println(result);
			} catch (Exception e) {
				System.out.println("Error " + e);
			}

		}
	}

}
