package csv; 

import java.sql.*;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;


public class Csv {
	private String sqlCommand = "";	
	private String fileName = "";
	private String ipAddress = "";
	private String userName = "";
	private String password = "";

	/**
	 * Constructs an instance of <code>iseriesutil.csv</code>.
	 */
	public Csv() {
		System.out.println("Constructor");
	}
	
	/**
	 * Gets the SQL Command.
	 * 
	 * @return String SQL Command
	 */
	public String getSqlCommand() {
		return sqlCommand;
	}

	/**
	 * Sets the SQL command making sure it is not blank and also making sure the
	 * command begins with 'SELECT'.
	 * 
	 * @param sqlCommand
	 *            The SQL Command
	 * @throws CsvException
	 */
	public void setSqlCommand(String sqlCommand) throws CsvException {
		System.out.println("setSqlCommand");
		if (sqlCommand.trim().equals("")) {
			throw new CsvException("SQL command cannot be blank.");
		} else if (!sqlCommand.trim().substring(0, 6).equalsIgnoreCase("SELECT")) {
			//throw new CsvException("SQL command must begin with 'SELECT'.");
		}
		this.sqlCommand = sqlCommand;
	}

	/**
	 * Gets the File Name to create.
	 * 
	 * @return String File name to create
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the File name to create.
	 * 
	 * @param fileName
	 *            File name to create.
	 * @throws CsvException
	 */
	public void setFileName(String fileName) throws CsvException {
		if (sqlCommand.trim().equals("")) {
			throw new CsvException("Filename cannot be blank.");
		}
		this.fileName = fileName;
	}

	/**
	 * Gets the IP Address.
	 * 
	 * @return String IP Address
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * Sets the IP Address of the machine to connect to
	 * 
	 * @param ipAddress
	 *            The IP address of the machine to connect to
	 * @throws CsvException
	 */
	public void setIpAddress(String ipAddress) throws CsvException {
		if (ipAddress.trim().equals("")) {
			throw new CsvException("IP Address cannot be blank.");
		}
		this.ipAddress = ipAddress;
	}

	/**
	 * Gets the User Name.
	 * 
	 * @return String User Name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the User Name to use to connect to the machine
	 * 
	 * @param userName
	 *            The User Name to use to connect to the machine
	 * @throws CsvException
	 */
	public void setUserName(String userName) throws CsvException {
		if (userName.trim().equals("")) {
			throw new CsvException("User Name cannot be blank.");
		}
		this.userName = userName;
	}
	
	/**
	 * Gets the Password.
	 * 
	 * @return String Password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the Password to use to connect to the machine
	 * 
	 * @param password
	 *            The Password to use to connect to the machine
	 * @throws CsvException
	 */
	public void setPassword(String password) throws CsvException {
		if (password.trim().equals("")) {
			throw new CsvException("Password cannot be blank.");
		}
		this.password = password;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Creates the CSV file by executing the SQL Command.
	 * 
	 * @throws CsvException
	 */
	public String createCsv() throws CsvException {
		String result = "";

		if (this.getSqlCommand().trim().equals("")) {
			throw new CsvException("SQL command cannot be blank.");
		}
		if (this.getFileName().trim().equals("")) {
			throw new CsvException("Filename cannot be blank.");
		}
		if (this.getIpAddress().trim().equals("")) {
			throw new CsvException("IP Address cannot be blank.");
		}
		if (this.getUserName().trim().equals("")) {
			throw new CsvException("User Name cannot be blank.");
		}
		if (this.getPassword().trim().equals("")) {
			throw new CsvException("Password cannot be blank.");
		}

		try {
			System.out.println("Connection conn = null;");
			Connection conn = null;
			String url = "jdbc:as400://" + ipAddress.trim() + "/;naming=system";

			System.out.println("Class.forName('com.ibm.as400.access.AS400JDBCDriver').newInstance();");
			Class.forName("com.ibm.as400.access.AS400JDBCDriver").newInstance();

			System.out.println("conn = DriverManager.getConnection(" + url + "," + userName + "," + password + ");");
			conn = DriverManager.getConnection(url, userName, password);

			System.out.println("conn.setReadOnly(true);");
			conn.setReadOnly(true);

			System.out.println("if (conn != null) {");
			if (conn != null) {
				// Generate and execute the SQL statement
				Statement st = conn.createStatement();
				st.execute(this.getSqlCommand());
				
				System.out.println("SQL Statement = ");
				System.out.println(this.getSqlCommand());

				// Load the result set
				ResultSet rs = st.getResultSet();

				// Get the Meta Data so we can see the field type of each column
				ResultSetMetaData md = rs.getMetaData();

				// Determine the number of columns that have been fetched
				int numCols = md.getColumnCount();

				try {
					FileWriter writer = new FileWriter(this.getFileName());

					// Print out the column headings
					for (int i = 1; i <= numCols; i++) {
						if (i > 1)
							writer.write(",");
						writer.write(md.getColumnName(i));
					}

					// Force a new line now that we have finished with the
					// heading
					writer.write("\n");

					// Print out all of the data
					while (rs.next()) {
						for (int i = 1; i <= numCols; i++) {
							String ms = rs.getString(i);
							//System.out.println("String = " + ms);
							if (ms!=null) {
								if (i > 1)
									writer.write(",");
								String columnType = md.getColumnTypeName(i);
								if (columnType.equalsIgnoreCase("NUMERIC") || columnType.equalsIgnoreCase("DECIMAL")) {
									writer.write(rs.getString(i).trim());
								}else {
									writer.write(formatLine(rs.getString(i).trim()));
								}
							}else{
								writer.write(",");
							}
						}
						writer.write("\n");
					}

					writer.flush();
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
					result = getStackTrace(e);
				}

			}
		} catch (Exception e) {
			System.out.println();
			System.out.println();
			System.out.println("Error " + e);
			result = getStackTrace(e);
		}
		return result;
	}

	/**
	 * Formats the error stack caught during an exception.
	 * 
	 * @param aThrowable
	 * 
	 * @return String Error Stack
	 */
	static String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString();
	}

	
	  
	    private static String formatLine(String columnString) {
	        StringBuffer s2 = new StringBuffer();
	                s2.append("\"" + columnString + "\"");
	        return s2.toString();
	    }	  
	  
	  
}
