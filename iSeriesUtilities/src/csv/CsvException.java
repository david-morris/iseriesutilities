package csv;

public class CsvException extends java.lang.Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2196922257136537171L;

	/**
	 * Creates a new instance of <code>EmailException</code> without detail
	 * message.
	 */
	public CsvException() {
	}

	/**
	 * Constructs an instance of <code>EmailException</code> with the specified
	 * detail message.
	 * 
	 * @param msg
	 *            The detail message.
	 */
	public CsvException(String msg) {
		super(msg);
	}
}
