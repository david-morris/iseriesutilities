package csv;

public class CsvTest {
	public static void main(String[] args) {

		String unused = "";
		System.out.println(unused);
		
		if (args.length < 5) {
			System.out.println("Five parameters required.");
			System.out.println("  1. IP Address");
			System.out.println("  2. User Name");
			System.out.println("  3. Password");
			System.out.println("  4. SQL Select Statement");
			System.out.println("  5. Name of CSV file to create");
		} else {

			try {
				Csv csv = new Csv();
				csv.setIpAddress(args[0]);
				csv.setUserName(args[1]);
				csv.setPassword(args[2]);
				csv.setSqlCommand(args[3]);
				csv.setFileName(args[4]);
				csv.createCsv();
			} catch (CsvException e) {
				System.out.println("Error " + e);
			}

		}

	}

}
