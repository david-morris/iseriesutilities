package splftopdf;

public class SplfToPdfException extends java.lang.Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8053709076466997567L;

	/**
	 * Creates a new instance of <code>SplfToPdfException</code> without detail
	 * message.
	 */
	public SplfToPdfException() {
	}

	/**
	 * Constructs an instance of <code>SplfTopdfException</code> with the specified
	 * detail message.
	 * 
	 * @param msg
	 *            The detail message.
	 */
	public SplfToPdfException(String msg) {
		super(msg);
	}
}