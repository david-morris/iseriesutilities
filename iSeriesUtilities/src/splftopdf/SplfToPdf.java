package splftopdf;

import java.beans.PropertyVetoException;
import java.io.*;

//import org.bouncycastle.util.Arrays;

import com.ibm.as400.access.*;
import com.lowagie.text.*;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;

public class SplfToPdf extends Object {
	private String pdfName;
	private String rows;
	private String cols;
	private String footer;
	private String ipAddress;
	private String userName;
	private String password;
	private String splfName;
	private String splfNumber;
	private String jobName;
	private String jobUser;
	private String jobNumber;


	/**
	 * Gets the name of the PDF file to create.
	 * 
	 * @return String
	 */
	public String getPdfName() {
		return this.pdfName;
	}

	/**
	 * Sets the name of the PDF file to create.
	 * 
	 * @param _pdfName
	 *            The name of the PDF file to create.
	 */
	public void setPdfName(String _pdfName) {
		this.pdfName = _pdfName;
	}

	/**
	 * Gets the number of rows (the height) of the spooled file.
	 * 
	 * @return String
	 */
	public String getRows() {
		return this.rows;
	}

	/**
	 * Sets the number of rows (the height) of the spooled file.
	 * 
	 * @param _rows
	 *            The number of rows (the height) of the spooled file.
	 */
	public void setRows(String _rows) {
		this.rows = _rows;
	}

	/**
	 * Gets the number of columns (the width) of the spooled file.
	 * 
	 * @return String
	 */
	public String getCols() {
		return this.cols;
	}

	/**
	 * Sets the number of columns (the width) of the spooled file.
	 * 
	 * @param _cols
	 *            The number of columns (the width) of the spooled file.
	 */
	public void setCols(String _cols) {
		this.cols = _cols;
	}

	/**
	 * Gets the comment to be written to the footer of the generated PDF
	 * document.
	 * 
	 * @return String
	 */
	public String getFooter() {
		return this.footer;
	}

	/**
	 * Sets the comment to be written to the footer of the generated PDF
	 * document.
	 * 
	 * @param _footer
	 *            The comment to be written to the footer of the generated PDF
	 *            document.
	 */
	public void setFooter(String _footer) {
		this.footer = _footer;
	}

	/**
	 * Gets the IP Address of the iSeries where the spooled file resides.
	 * 
	 * @return String
	 */
	public String getIpAddress() {
		return this.ipAddress;
	}

	/**
	 * Sets the IP Address of the iSeries where the spooled file resides.
	 * 
	 * @param _ipAddress
	 *            The IP Address of the iSeries where the spooled file resides.
	 */
	public void setIpAddress(String _ipAddress) {
		this.ipAddress = _ipAddress;
	}

	/**
	 * Gets the user name to use to connect to the iSeries where
	 * the spooled file resides.
	 * 
	 * @return String
	 */
	public String getUserName() {
		return this.userName;
	}

	/**
	 * Sets the user name to use to connect to the iSeries where
	 * the spooled file resides.
	 * 
	 * @param _userName
	 *            The user name to use to connect to the iSeries
	 *            where the spooled file resides.
	 */
	public void setUserName(String _userName) {
		this.userName = _userName;
	}

	/**
	 * Gets the password to use to connect to the iSeries where
	 * the spooled file resides.
	 * 
	 * @return String
	 */
	public String getPassowrd() {
		return this.password;
	}

	/**
	 * Sets the password to use to connect to the iSeries where
	 * the spooled file resides.
	 * 
	 * @param _password
	 *            The password to use to connect to the iSeries where
	 *            the spooled file resides.
	 */
	public void setPassword(String _password) {
		this.password = _password;
	}

	/**
	 * Gets the name of the spooled file name on the iSeries that is to 
	 * be converted into a PDF document.
	 * 
	 * @return String
	 */
	public String getSplfName() {
		return this.splfName;
	}

	/**
	 * Sets the name of the spooled file on the iSeries that is to 
	 * be converted into a PDF document.
	 * 
	 * @param _splfName
	 *            The name of the spooled file name on the iSeries 
	 *            that is to be converted into a PDF document.
	 */
	public void setSplfName(String _splfName) {
		this.splfName = _splfName;
	}

	/**
	 * Gets the number of the spooled file on the iSeries that is to
	 * be converted into a PDF document.
	 * 
	 * @return String
	 */
	public String getSplfNumber() {
		return this.splfNumber;
	}

	/**
	 * Sets the number of the spooled file on the iSeries that is to
	 * be converted into a PDF document.
	 * 
	 * @param _splfNumber
	 *            The number of the spooled file on the iSeries that is
	 *            to be converted into a PDF document.
	 */
	public void setSplfNumber(String _splfNumber) {
		this.splfNumber = _splfNumber;
	}
	
	/**
	 * Gets the name of the job that created the spooled file.
	 * 
	 * @return String
	 */
	public String getJobName() {
		return this.jobName;
	}

	/**
	 * Sets the name of the job that created the spooled file.
	 * 
	 * @param _jobName
	 *            The name of the job that created the spooled file.
	 */
	public void setJobName(String _jobName) {
		this.jobName = _jobName;
	}
	
	/**
	 * Gets the user of the job that created the spooled file.
	 * 
	 * @return String
	 */
	public String getJobUser() {
		return this.jobUser;
	}

	/**
	 * Sets the user of the job that created the spooled file.
	 * 
	 * @param _jobUser
	 *            The user of the job that created the spooled file.
	 */
	public void setJobUser(String _jobUser) {
		this.jobUser = _jobUser;
	}
	
	/**
	 * Gets the number of the job (the job number) that created the 
	 * spooled file.
	 * 
	 * @return String
	 */
	public String getJobNumber() {
		return this.jobNumber;
	}

	/**
	 * Sets the number of the job (the job number) that created the 
	 * spooled file.
	 * 
	 * @param _jobNumber
	 *            The number of the job (the job number) that created 
	 *            the spooled file.
	 */
	public void setJobNumber(String _jobNumber) {
		this.jobNumber = _jobNumber;
	}
	
	/**
	 * Create the PDF document.
	 * 
	 * @return result
	 * @throws SplfToPdfException
	 */
	public String createPdf() throws SplfToPdfException {
		String result = "";
		
		// Create the output stream
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(this.pdfName);

			// Connect to the iSeries
			AS400 iSeriesServer = new AS400(ipAddress);
			iSeriesServer.setUserId(userName);
			iSeriesServer.setPassword(password);
			iSeriesServer.setGuiAvailable(false);
			
			// Convert the spooled file number into an integer
			int splfNumberInt = 0;
			if (splfNumber != null) {
				splfNumberInt = Integer.parseInt(splfNumber, 10);
			}
			
			// Generate the Spooled File object
			SpooledFile splf = null;
			splf = new SpooledFile(iSeriesServer, 
					this.splfName, 
					splfNumberInt, 
					this.jobName, 
					this.jobUser, 
					this.jobNumber);

			// Read the spooled file attributes for determining if it is AFPDS
			// or SCS type spool file
			if ((splf.getStringAttribute(PrintObject.ATTR_PRTDEVTYPE) != null)
  			 && (splf.getStringAttribute(PrintObject.ATTR_PRTDEVTYPE).equals("*AFPDS"))) {
				convertAFPDStoPDF(splf, fos);
				
			} else if ((splf.getStringAttribute(PrintObject.ATTR_PRTDEVTYPE) != null)
					&& (splf.getStringAttribute(PrintObject.ATTR_PRTDEVTYPE).equals("*SCS"))) {
				result = convertSCStoPDF(splf, fos, this.pdfName);
				//convertSCStoPDFOriginal(splf, fos);
				//testCT();
				//result = asposePDF(splf, fos);
			}

			// Close the output stream
			fos.close();
			
		} catch (FileNotFoundException e) {
			result = "PDF file location is not valid. (" + this.pdfName + ")";			
		} catch (PropertyVetoException e) {
			result = "Not valid system name or user id or password";
		} catch (IOException e) {
			result = getStackTrace(e);
		} catch (Exception e) {
			result = getStackTrace(e);
		}
		
		return result;		
	}	
	

	/**
	 * Formats the error stack caught during an exception.
	 * 
	 * @param aThrowable
	 * 
	 * @return String Error Stack
	 */
	static String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString();
	}


	/**
	 * Convert a SCS spooled file into a PDF.
	 * 
	 * @param splf	Spooled File Object.
	 * @param fs	Output file to create (the PDF document).
	 */
	private static String convertSCStoPDF(SpooledFile _splf, FileOutputStream _fos, String _pdfName) {

		String result = "";
		
		try {
			// Create the Print Parameter List object and set some properties
			PrintParameterList ppl = new PrintParameterList();
			ppl.setParameter(PrintObject.ATTR_MFGTYPE, "*WSCST");
			ppl.setParameter(PrintObject.ATTR_WORKSTATION_CUST_OBJECT, "/QSYS.LIB/QWPDEFAULT.WSCST");
	
			// Create the input stream
			PrintObjectTransformedInputStream inpStream = null;
			inpStream = _splf.getTransformedInputStream(ppl);
			
			// Read the input stream buffer and create a string buffer
			// which will hold the entire spooled file contents.
			byte[] buf = new byte[32767];
			StringBuffer buffer = new StringBuffer();
			int bytesRead = 0;
			do {
				bytesRead = inpStream.read(buf);
				if (bytesRead > 0) {
					buffer.append(new String(buf, 0, bytesRead));
				}
			} while (bytesRead != -1);
			
			// Set the margins to use on the generated PDF document.
			float leftMargin = 14f;
			float rightMargin = 7f;
			float topMargin = 7f;
			float bottomMargin = 7f;
			
			// Create the document and the writer objects
			Document document = new Document(PageSize.A4.rotate(), leftMargin, rightMargin,	topMargin, bottomMargin);
			PdfWriter writer = PdfWriter.getInstance(document, _fos);
			
			// Get some attributes of the spooled file
			float attr_cpi = _splf.getFloatAttribute(SpooledFile.ATTR_CPI).floatValue();
			float attr_pagewidth = _splf.getFloatAttribute(SpooledFile.ATTR_PAGEWIDTH).floatValue();
			
			// The maximum font size allowed is 10pt (12 CPI)
			if (attr_cpi < 12) {
				attr_cpi = 12;
			}
			
			// At 12CPI we can only fit 132 characters, so if wider than that then
			// we need to shrink down the font size (make CPI larger).
			int MaxWidth1 = 132;
			int MaxWidth2 = 143;
			int MaxWidth3 = 170;
			if (attr_pagewidth > 0 && attr_pagewidth <=MaxWidth1) {
				attr_cpi = 12;
			}else if(attr_pagewidth > MaxWidth1 && attr_pagewidth <=MaxWidth2) {
				attr_cpi = 13;					
			}else if(attr_pagewidth > MaxWidth2 && attr_pagewidth <=MaxWidth3) {
				attr_cpi = 15;					
			}else if (attr_pagewidth > MaxWidth3) {
				attr_cpi = 18;
			}
			
			// Set the font "size" based on the CPI
			float size = (120 / attr_cpi);

			// Set the font to be used
			Font courierFont = FontFactory.getFont(FontFactory.COURIER, size);
			
			// Open the iText document object
			document.open();
			
			// Set starting X and Y positions on page (starting at bottom left corner of page)
			float yPosition = 570.0f;
			float xPosition = 10.0f;
			int lineHeight = 8;

			// Split the Spooled File into pages and loop through each page
			String [] pages = buffer.toString().split("\f");					
			for (int i = 0; i < pages.length ; i++) {

				// Set starting yPosition
				yPosition = 570.0f;
				
				// Force new page
				if (i>0) document.newPage();

				// Split the page into lines and loop through all lines
				String [] lines = pages[i].split("\n");
				for (int x = 0 ; x <lines.length; x++){

					yPosition = yPosition - lineHeight;

					// Split the line by the return character because this is how
					// the iSeries handles BOLD via \r and over-printing
					// If no \r's in the line, we will still have one element
					String [] lines2 = lines[x].split("\r");
					for (int y = 0 ; y < lines2.length ; y++){

						if (y==0) {
							xPosition = 10.0f;
						}else{
							// Move each BOLD line across a little bit
							xPosition=10.0f;
						}
						
						// If this line is empty, set it to one blank character because 
						// iText does not output an empty paragraph properly
						if (lines2[y].length()==0) {
							lines2[y]=" ";
						}
						
						// Add the line of text to the page
						Phrase p = new Phrase(lines2[y], courierFont);
						PdfContentByte canvas = writer.getDirectContent();						
						ColumnText.showTextAligned(canvas,  Element.ALIGN_LEFT,  p,  xPosition,  yPosition,  0);
					}
					
				}
			}
			result = "Created " + _pdfName;
			document.close();
			
		// Handle all AS/400 exceptions	
		} catch (AS400Exception e) {
			result = getStackTrace(e);
		} catch (AS400SecurityException e) {
			result = getStackTrace(e);
		} catch (ErrorCompletingRequestException e) {
			result = getStackTrace(e);
		} catch (RequestNotSupportedException e) {
			result = getStackTrace(e);
			
		// Handle iText document exceptions
		} catch (DocumentException e) {
			result = getStackTrace(e);		

		// Handle Java IO Exceptions
		} catch (IOException e) {
			result = getStackTrace(e);
		} catch (InterruptedException e) {
			result = getStackTrace(e);
			
		// Handle all other generic exceptions
		} catch (Exception e) {
			result = getStackTrace(e);
		}
		
		return result;
	}
	
	
	/**
	 * Convert an AFPDS spooled file into a PDF.
	 * 
	 * @param splf	Spooled File Object.
	 * @param fs	Output file to create (the PDF document).
	 */
	private static void convertAFPDStoPDF(SpooledFile splf, FileOutputStream fs) {

		try {
			PrintParameterList prtParm = new PrintParameterList();
			prtParm.setParameter(PrintObject.ATTR_MFGTYPE, "*WSCST");
			prtParm.setParameter(PrintObject.ATTR_WORKSTATION_CUST_OBJECT, "/QSYS.LIB/QWPGIF.WSCST");

			PrintObjectTransformedInputStream inpStream = null;
			inpStream = splf.getTransformedInputStream(prtParm);

			// Set the margins to use on the generated PDF document.
			float leftMargin = 4f;
			float rightMargin = 4f;
			float topMargin = 4;
			float bottomMargin = 4;

			try {
				// Create the iText document object (in landscape mode)
				Document document = new Document(PageSize.A4.rotate(), leftMargin, rightMargin,
						topMargin, bottomMargin);
				try {
					PdfWriter.getInstance(document, fs);
				} catch (DocumentException e1) {
					System.err.println("document error happened. Unable to continue");
					e1.printStackTrace();
					return;
				}

				byte[] imagebuff = null;
				try {
					// Read the input stream and create an image
					imagebuff = new byte[inpStream.available()];
					inpStream.read(imagebuff);
					Image gifImage = Image.getInstance(imagebuff);
					
					// Open the iText document object, add the image and close 
					// the document object.
					document.open();
					document.add(gifImage);
					document.close();

				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (DocumentException e) {
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}		
}