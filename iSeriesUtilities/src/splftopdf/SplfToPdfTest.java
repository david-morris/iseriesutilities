package splftopdf;

public class SplfToPdfTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//String r1 = "c:/temp/un568.pdf 00 000 footer 10.81.0.74 dmorris vpasword2 UN568 00001 AGNTSALES qvtcxe 099040";
		//String r1 = "c:/temp/dr800.pdf 00 000 footer 10.81.0.74 dmorris vpasword2 DR800 00004 COMMREQUIS accsrv 917445";
		//String r1 = "c:/temp/dr909.pdf 00 000 footer 10.81.0.74 dmorris vpasword2 DR909 00003 CREDITBALR accsrv 936943";
		//String r1 = "c:/temp/wrksyssts.pdf 00 000 footer 10.81.0.74 dmorris vpasword2 QPDSPSTS 000013 QVTU01MIT1 dmorris 950103";
		//String r1 = "c:/temp/test132.pdf 00 000 footer 10.81.0.74 dmorris vpasword2 QPRINT 000024 QVTU01MIT1 DMORRIS 950980";
		//String r1 = "c:/temp/test170.pdf 00 000 footer 10.81.0.74 dmorris vpasword2 QPRINT2 000023 QVTU01MIT1 DMORRIS 950980";
		String r1 = "c:/temp/test198.pdf 00 000 footer 10.81.0.74 dmorris vpasword2 QPRINT3 000022 QVTU01MIT1 DMORRIS 950980";
		args = r1.split(" ");
		
		if (args.length == 12 ){
			try { 
				String pdfName = args[0];
				String rows = args[1];
				String cols = args[2];
				String footer = args[3];
				String ipAddress = args[4];
				String userName = args[5];
				String password = args[6];
				String splfName = args[7];
				String splfNumber = args[8];
				String jobName = args[9];
				String jobUser = args[10];
				String jobNumber = args[11];

				SplfToPdf s = new SplfToPdf();
				s.setPdfName(pdfName);
				s.setRows(rows);
				s.setCols(cols);
				s.setFooter(footer);
				s.setIpAddress(ipAddress);
				s.setUserName(userName);
				s.setPassword(password);
				s.setSplfName(splfName);
				s.setSplfNumber(splfNumber);
				s.setJobName(jobName);
				s.setJobUser(jobUser);
				s.setJobNumber(jobNumber);
				String result = s.createPdf();
				System.out.println(result);
			}  catch (Exception e) {
				System.out.println("Could not read the work file");
				System.out.println(e.getMessage());
				System.exit(0);
			}

		} else {
			System.out.println("IseriesUtilities SplfToPdf test wrapper");
			System.out.println("---------------------------------------");
			System.out.println("");
			System.out.println("Usage:   iseriesutilities.spltopdf.SplfToPdfTest <path> <name> <rows> <cols> <notes> <library> <file> <member>");
			System.out.println("");
			System.out.println("Example: iseriesutilities.spltopdf.SplfToPdfTest /mypdf order 66 198 Demo qgpl tmpspool m000001");
			System.out.println("");
		}

	}

}
